
//things left to do, add characters and balancing.

//===========================GLOBAL VARIABLES============================

var world = ['allies', 'mid', 'demons']
var armies = []
var demonArmies = []
var battleNum = 0
var gold = 0
var gameWon = false
var gameLost = false
var current = 0
var totalWins = 0
var totalLosses = 0

//=====================ALLIES=========================

const allies = [
    {
        name: 'soldier',
        type: 'close combat',
        amount: 100,
        attackPower: 6,
        attackNum: 3,
        defence: 4,
        magDef: 2,
        lvl: 1,
        exp: 0,
        have: true,
        dead: false,
        item1: false,
        item2: false,
        hero: false,
        cost: 60,
        resetAttacks: 3
    },
    {
     name: 'soldier2',
     type: 'close combat',
     amount: 100,
     attackPower: 3,
     attackNum: 3,
     defence: 4,
     magDef: 2,
     lvl: 1,
     exp: 0,
     have: false,
     dead: false,
     item1: false,
     item2: false,
     hero: false,
     cost: 50,
     resetAttacks: 3
 },
    {
        name: 'wizard',
        type: 'magic attack',
        amount: 100,
        magAttackPower: 6,
        attackPower: 2,
        attackNum: 3,
        defence: 4,
        magDef: 5,
        lvl: 1,
        exp: 0,
        have: true,
        dead: false,
        item1: false,
        item2: false,
        hero: false,
        cost: 50,
        resetAttacks: 3
    },
    {
     name: 'wizard2',
     type: 'magic attack',
     amount: 100,
     magAttackPower: 4,
     attackPower: 2,
     attackNum: 3,
     defence: 4,
     magDef: 5,
     lvl: 1,
     exp: 0,
     have: false,
     dead: false,
     item1: false,
     item2: false,
     hero: false,
     cost: 50,
     resetAttacks: 3
 },
    {
     name: 'archer',
     type: 'ranged',
     amount: 100,
     attackPower: 6,
     attackNum: 3,
     defence:  2,
     magDef: 2,
     lvl: 1,
     exp: 0,
     have: true,
     dead: false,
     item1: false,
     item2: false,
     hero: false,
     cost: 50,
     resetAttacks: 3
    },
    {
     name: 'archer2',
     type: 'ranged',
     amount: 100,
     attackPower: 5,
     attackNum: 3,
     defence:  2,
     magDef: 2,
     lvl: 1,
     exp: 0,
     have: false,
     dead: false,
     item1: false,
     item2: false,
     hero: false,
     cost: 50,
     resetAttacks: 3
 },
    {
        name: 'heavyInfantry',
        type: 'close combat',
        amount: 100,
        attackPower: 8,
        attackNum: 2,
        defence:  6,
        magDef: 2,
        lvl: 1,
        exp: 0,
        have: false,
        dead: false,
        item1: false,
        item2: false,
        hero: false,
        cost: 75,
        resetAttacks: 2
    },
    {
     name: 'heavyInfantry2',
     type: 'close combat',
     amount: 100,
     attackPower: 6,
     attackNum: 2,
     defence:  6,
     magDef: 2,
     lvl: 1,
     exp: 0,
     have: false,
     dead: false,
     item1: false,
     item2: false,
     hero: false,
     cost: 75,
     resetAttacks: 2
 },
    ]

//====================DEMONS=========================

var defaultDemons = [
    {
        name: 'lesserDemon',
        amount: 100,
        attackPower: 3,
        magAttackPower: 2,
        attackNum: 5,
        defence:  3,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 50
    },
    {
        name: 'lesserDemon2',
        amount: 100,
        attackPower: 3,
        magAttackPower: 2,
        attackNum: 5,
        defence:  3,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 50
    },
    {
        name: 'lesserDemon3',
        amount: 100,
        attackPower: 3,
        magAttackPower: 2,
        attackNum: 5,
        defence:  3,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 50
    },
    {
        name: 'greaterDemon',
        amount: 100,
        attackPower: 4,
        magAttackPower: 3,
        attackNum: 3,
        defence:  6,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 100
    },
    {
        name: 'greaterDemon2',
        amount: 100,
        attackPower: 4,
        magAttackPower: 3,
        attackNum: 3,
        defence:  6,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 100
    },
    {
        name: 'greaterDemon3',
        amount: 100,
        attackPower: 4,
        magAttackPower: 3,
        attackNum: 3,
        defence:  6,
        magDef: 3,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 100
    },
    {
        name: 'balrog',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    },
    {
        name: 'balrog2',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    },
    {
        name: 'balrog3',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    },
    {
        name: 'balrog4',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    },
    {
        name: 'balrog5',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    },
    {
        name: 'balrog6',
        amount: 100,
        attackPower: 8,
        magAttackPower: 5,
        attackNum: 2,
        defence:  5,
        magDef: 5,
        lvl: 1,
        exp: 0,
        dead: false,
        item: false,
        dreadlord: false,
        used: false,
        gold: 150
    }
]

var demons = [
{
    name: 'lesserDemon',
    amount: 100,
    attackPower: 3,
    magAttackPower: 2,
    attackNum: 3,
    defence:  3,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 50
},
{
    name: 'lesserDemon2',
    amount: 100,
    attackPower: 3,
    magAttackPower: 2,
    attackNum: 3,
    defence:  3,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 50
},
{
    name: 'lesserDemon3',
    amount: 100,
    attackPower: 3,
    magAttackPower: 2,
    attackNum: 3,
    defence:  3,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 50
},
{
    name: 'greaterDemon',
    amount: 100,
    attackPower: 4,
    magAttackPower: 3,
    attackNum: 3,
    defence:  6,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 100
},
{
    name: 'greaterDemon2',
    amount: 100,
    attackPower: 4,
    magAttackPower: 3,
    attackNum: 3,
    defence:  6,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 100
},
{
    name: 'greaterDemon3',
    amount: 100,
    attackPower: 4,
    magAttackPower: 3,
    attackNum: 3,
    defence:  6,
    magDef: 3,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 100
},
{
    name: 'balrog',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
},
{
    name: 'balrog2',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
},
{
    name: 'balrog3',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
},
{
    name: 'balrog4',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
},
{
    name: 'balrog5',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
},
{
    name: 'balrog6',
    amount: 100,
    attackPower: 8,
    magAttackPower: 5,
    attackNum: 2,
    defence:  5,
    magDef: 5,
    lvl: 1,
    exp: 0,
    dead: false,
    item: false,
    dreadlord: false,
    used: false,
    gold: 150
}
]

// //=============================ITEMS======================================

var items = [
    {
        name: 'whetstone',
        attackBonus: 2,
        used: false,
        have: true,
        type: 'close combat',
        cost: 50
    },
    {
        name: 'sharp arrows',
        attackBonus: 2,
        used: false,
        have: true,
        type: 'ranged',
        cost: 50
    },
    {
        name: 'staff',
        magAttBonus: 2,
        used: false,
        have: true,
        type: 'magic attack',
        cost: 50
    },
    {
        name: 'leather armor',
        defenceBonus: 2,
        used: false,
        have: true,
        type: 'defence',
        cost: 50
    },
    {
        name: 'armor',
        defenceBonus: 2,
        used: false,
        have: true,
        type: 'defence',
        cost: 50
    },
    {
        name: 'magic armor',
        magDefBonus: 2,
        used: false,
        have: true,
        type: 'defence',
        cost: 50
    }
]

//=========================HEROES=============================================
var heroes = [
    {
        name: 'demon slayer',
        attackBonus: 1,
        defenceBonus: .5,
        attackNumBonus: 1,
        used: false,
        have: true,
        cost: 100
    },
    {
        name: 'randy',
        magAttBonus: 1,
        magDefBonus: 1,
        used: false,
        have: true,
        cost: 100
    }
]

//=============================CHOOSE ARMY================================

var chooseArmies = () => {
    
        while (armies.length < 3) {
            let req = prompt('choose a unit for your army')
            let unitIndex = allies.findIndex(item => item.name == req)
            let armyIndex = armies.findIndex(item => item.name == req)

            if (unitIndex === -1) {
                console.log('unit doesnt exist')
                
            }
            else if (allies[unitIndex].dead === true) {
                console.log('this unit is dead')
                
            }
            else if (allies[unitIndex].have === false) {
                console.log('you dont have this unit yet')
                
            }
            else if (unitIndex !== -1 && armyIndex === -1) {
                armies.push(allies[unitIndex])
                console.log(`${req} has been added to your army`)
                
            } 
            else if (armyIndex !== -1) {
                console.log(`${req} is already in your army`)
                
            }
        }
        return console.log(`You have chosen ${armies[0].name}, ${armies[1].name}, ${armies[2].name}`)
    }
    
//================================EQUIP ITEMS==============================

var equipItems = () => {
    let num = 0
    while (num < 6) {
        for (var unit of armies) {
        if (unit.item1 === false || unit.item1 != 'none') {
            let req = prompt(`Equip an item for ${unit.name}?`)
            let itemIndex = items.findIndex(item => item.name == req)
            //cant be true if answer is no because no is not an item
            if (req == 'no') {
                unit.item1 = 'none'
                num++
                console.log(`${unit.name} has no item equiped in slot 1.`)
                }
            else if (unit.type !== items[itemIndex].type && items[itemIndex].type !== 'defence') {
                console.log(`${unit.name} cant equip this type of item`)
            }
            else if (itemIndex === -1) {
                console.log('Item doesnt exist.')
                }
            else if(items[itemIndex].used === true) {
                console.log('None of these items left.')
            } 
            else if (itemIndex !== -1 && items[itemIndex].used === false) {
                num++
                unit.item1 = items[itemIndex]
                console.log(`${unit.name} has been equiped with ${items[itemIndex].name}`)
                items[itemIndex].used = true
                // apply bonus to unit
                if (items[itemIndex].attackBonus) {
                    unit.attackPower += items[itemIndex].attackBonus
                }
                else if (items[itemIndex].magAttBonus) {
                    unit.magAttackPower += items[itemIndex].magAttBonus
                }
                else if (items[itemIndex].defenceBonus) {
                    unit.defence += items[itemIndex].defenceBonus
                }
                else if (items[itemIndex].magDefBonus) {
                    unit.magDef += items[itemIndex].magDefBonus
                }
            } 
        }
        if (unit.item2 === false || unit.item2 != 'none') {
            let req = prompt(`Equip a second item for ${unit.name}?`)
            let itemIndex2 = items.findIndex(item => item.name == req)
            //cant be true if answer is no because no is not an item
            if (req == 'no') {
                unit.item2 = 'none'
                num++
                console.log(`${unit.name} has no second item equiped in slot 2.`)
                }
            else if (unit.type !== items[itemIndex2].type && items[itemIndex2].type !== 'defence') {
                console.log(`${unit.name} cant equip this type of item`)
            }
            else if (itemIndex2 === -1) {
                console.log('Item doesnt exist.')
                }
            else if(items[itemIndex2].used === true) {
                console.log('None of these items left.')
            } 
            else if (itemIndex2 !== -1 && items[itemIndex2].used === false) {
                num++
                unit.item2 = items[itemIndex2]
                console.log(`${unit.name} has been equiped with ${items[itemIndex2].name}`)
                items[itemIndex2].used = true
                // apply bonus to unit
                if (items[itemIndex2].attackBonus) {
                    unit.attackPower += items[itemIndex2].attackBonus
                }
                else if (items[itemIndex2].magAttBonus) {
                    unit.magAttackPower += items[itemIndex2].magAttBonus
                }
                else if (items[itemIndex2].defenceBonus) {
                    unit.defence += items[itemIndex2].defenceBonus
                }
                else if (items[itemIndex2].magDefBonus) {
                    unit.magDef += items[itemIndex2].magDefBonus
                }
            } 
        } 
    } 
}
return armies
}

//=============================EQUIP HEROES=====================================

var equipHeroes = () => {
    let num = 0

    while(num < 3) {
        for(var unit of armies) {
        if (unit.hero === false && unit.hero != 'none') {
            let req = prompt(`Equip a hero for ${unit.name}?`)
            let heroIndex = heroes.findIndex(item => item.name == req)
//cant be true if answer is no because no is not an item
            if (req == 'no') {
                unit.hero = 'none'
                num++
                console.log(`${unit.name} has no hero equiped`)
                }
            else if (heroIndex === -1) {
                console.log('Hero doesnt exist')
            }
            else if(heroes[heroIndex].used === true) {
                console.log(`${heroes[heroIndex].name} is already in a unit.`)
            } 
            else if (heroIndex !== -1 && req != 'no' && heroes[heroIndex].used === false) {
                num++
                unit.hero = heroes[heroIndex]
                console.log(`${unit.name} has been equiped with ${heroes[heroIndex].name}`)
                unit.hero === true
                heroes[heroIndex].used = true
                // apply bonus to unit
                if (unit.hero.name == 'randy') {
                    unit.magAttackPower += unit.hero.magAttBonus
                    unit.magDef += unit.hero.magDefBonus
                }
                else if (unit.hero.name == 'demon slayer') {
                    unit.attackPower += unit.hero.attackBonus
                    unit.defence += unit.hero.defenceBonus
                    unit.attackNum += unit.hero.attackNumBonus
                }
            } 
        } 
    }
}
    return armies

}

//============================AUTOFILL DEMON ARMY================================

var randomDemonArmy = () => {

// need to impliment a system for war progress, choosing harder demons

if (battleNum < 3) {
    for (var i = 0; i < demonArmies.length+1; i++) {
        while (demonArmies.length < 3)  {
        let number = Math.round(Math.random() * 3)
        if (demonArmies.length < 3 && demonArmies[i] !== demons[number] && demons[number].used != true) {
                demonArmies.push(demons[number])
                demons[number].used = true       
                } 
            }
        }
        console.log(`Demon army ready ${demonArmies[0].name}, ${demonArmies[1].name}, ${demonArmies[2].name}.`)
    }
else if (battleNum > 2 && battleNum < 6) {
    for (var i = 0; i < demonArmies.length+1; i++) {
        while (demonArmies.length < 3)  {
        let number = Math.round(Math.random() * 6)
        if (number > 1 && demonArmies.length < 3 && demonArmies[i] !== demons[number] && demons[number].used != true) {
                demonArmies.push(demons[number])
                   demons[number].used = true       
                } 
            }
        }
        console.log(`Demon army ready ${demonArmies[0].name}, ${demonArmies[1].name}, ${demonArmies[2].name}.`)
    }
 else if (battleNum > 5 && battleNum < 9) {
    for (var i = 0; i < demonArmies.length+1; i++) {
        while (demonArmies.length < 3)  {
        let number = Math.round(Math.random() * 9)
        if (number > 4 && demonArmies.length < 3 && demonArmies[i] !== demons[number] && demons[number].used != true) {
                demonArmies.push(demons[number])
                demons[number].used = true       
                } 
            }
        }
        console.log(`Demon army ready ${demonArmies[0].name}, ${demonArmies[1].name}, ${demonArmies[2].name}.`)
    }
else if (battleNum > 8) {
    for (var i = 0; i < demonArmies.length+1; i++) {
        while (demonArmies.length < 3)  {
        let number = Math.round(Math.random() * 11)
        if (number > 6 && demonArmies.length < 3 && demonArmies[i] !== demons[number] && demons[number].used != true) {
                demonArmies.push(demons[number])
                demons[number].used = true       
                } 
            }
        }
        console.log(`Demon army ready ${demonArmies[0].name}, ${demonArmies[1].name}, ${demonArmies[2].name}.`)
    }
}

//==========================INITIALISE BATTLE=======================================

var initialiseWar = () => {
    current = world[1]
}

//=============================BATTLE ROUND=========================================

var battleRound = () => {
    let number = Math.round(Math.random() * 10)

    battleNum += 1

    let allAlliesDead = false
    
    let allDemonsDead = false 
   
    //get total number of attacks
    var totalAlliesAttacks = 0
    var totalDemonAttacks = 0
    var totalAlliesAmount = 0
    var totalDemonsAmount = 0

    for (var unit of armies) {
        totalAlliesAttacks += unit.attackNum
    }
    for (var unit of demonArmies) {
        totalDemonAttacks+= unit.attackNum
    }
    var totalAttacks = totalAlliesAttacks + totalDemonAttacks

    if (number > 5) {
        let counter = 0
        let allyCounter = 0
        let demonCounter = 0
        alert('Allies get first attack!')
        while (counter <= totalAttacks || allAlliesDead ==! true || allDemonsDead !== true) {
            if (armies[0].dead === true && armies[1].dead === true && armies[2].dead === true) {
                allAlliesDead = true
            }
            else if (demonArmies[0].dead === true && demonArmies[1].dead === true && demonArmies[2].dead === true) {
                allDemonsDead = true
            }
            //allies attack then demons attack until no attacks left or all enemies or allies are dead
            else if (allyCounter < totalAlliesAttacks || demonCounter < totalDemonAttacks) {
                chooseAttacker()
                allyCounter++
                counter++
                demonAttack()
                demonCounter++
                counter++
            }
            else if (allyCounter <= totalAlliesAttacks && demonCounter >= totalDemonAttacks) {
                alert('Enemies are out of attacks, destroy them!')
                chooseAttacker()
                allyCounter++
                counter += 2 
            }
            else if (allyCounter >= totalAlliesAttacks && demonCounter <= totalDemonAttacks) {
                alert("You're out of attacks")
                demonAttack()
                demonCounter++
                counter += 2
            }
           
            if(allAlliesDead === true) {
                alert('All ally armies have been slain, we lost the battle...')
                if (current === world[1]) {
                    current = world[0]
                    return alert('Next battle will take place at your home kingdom...')
                }
                else if (current === world[0]) {
                    gameLost = true
                   return alert('Demons have won the war, GAME OVER...')
                }
                else if (current === world[2]) {
                    current = world[1]
                    return alert('Demons deffended their fortress, next battle will take place on neutral grounds...')
                }
            }
            else if (allDemonsDead === true) {
                alert('All demons have been slain, we won this battle!')
                if (current === world[1]) {
                    current = world[2]
                    return alert('Next battle will take place at the Demons fortress!')
                }
                else if (current === world[2]) {
                    gameWon = true
                    return alert('Allies have destroyed all the demons and have won the war!!!')
                }
                else if (current === world[0]) {
                    return alert('Allies have deffended their kingdom! Next battle will take place at the neutral grounds.')
                }
            }
            else if (counter === totalAttacks) {
                for(var unit of armies) {
                    totalAlliesAmount += unit.amount
               }
               for(var unit of demonArmies) {
                   totalDemonsAmount += unit.amount
               }
               alert('both armies are out of attacks, battle over.')
               if (totalAlliesAmount > totalDemonsAmount) {
                if (current === world[1]) {
                    current = world[2]
                    return alert('Allies win this battle, next battle will be for the win at demon world')
                }
                else if (current === world[0]) {
                    current = world[1]
                    return alert('The allies have won and successfully deffended their kingdom! next battle will take place at the neutral ground')
                } else {
                    gameWon = true
                    return alert('Allies have defeated the demons and have won the war!!!')
                    
                }
             }
            else if(totalDemonsAmount > totalAlliesAmount) {
                if (current === world[1]) {
                    current = world[0]
                    return alert('Demons win this battle, next battle will be at Allies Kingdom...')
                }
                else if (current === world[2]) {
                    current = world[1]
                    return alert('The Demons have deffended their world... next battle will take place at the neutral ground')
                } else {
                    gameLost = true
                    return alert('The demons have won... We are all fucked...')                   
                }
             }
            }
        }
        
    }
    else {
        //demons attack, figure out who for them to attack and call the function
        let counter2 = 0
        let allyCounter = 0
        let demonCounter = 0
        alert('Demons get first attack...')   
        while (counter2 < totalAttacks || allAlliesDead ==! true || allDemonsDead !== true) {
            if (armies[0].dead === true && armies[1].dead === true && armies[2].dead === true) {
                allAlliesDead = true
            }
            else if (demonArmies[0].dead === true && demonArmies[1].dead === true && demonArmies[2].dead === true) {
                allDemonsDead = true
            }
            else if (allyCounter < totalAlliesAttacks || demonCounter < totalDemonAttacks) {
                demonAttack()
                demonCounter++
                counter2++
                chooseAttacker()
                allyCounter++
                counter2++
            }
            else if (allyCounter <= totalAlliesAttacks && demonCounter >= totalDemonAttacks) {
                chooseAttacker()
                allyCounter++
                counter2 += 2
            }
            else if (allyCounter >= totalAlliesAttacks && demonCounter <= totalDemonAttacks) {
                demonAttack()
                demonCounter++
                counter2 += 2
            }
            if(allAlliesDead === true) {
                alert('All ally armies have been slain, we lost the battle...')
                if (current === world[1]) {
                    current = world[0]
                    return alert('Next battle will take place at your home kingdom...')
                }
                else if (current === world[0]) {
                   return alert('Demons have won the war, GAME OVER...')
                }
                else if (current === world[2]) {
                    current = world[1]
                    return alert('Demons deffended their fortress, next battle will take place on neutral grounds...')
                }
            }
            else if (allDemonsDead === true) {
                alert('All demons have been slain, we won this battle!')
                if (current === world[1]) {
                    current = world[2]
                    return alert('Next battle will take place at the Demons fortress!')
                }
                else if (current === world[2]) {
                    return alert('Allies have destroyed all the demons and have won the war!!!')
                }
                else if (current === world[0]) {
                    return alert('Allies have deffended their kingdom! Next battle will take place at the neutral grounds.')
                }
            }
            else if (counter2 === totalAttacks) {
                for(let unit of armies) {
                    totalAlliesAmount += unit.amount
               }
               for(let unit of demonArmies) {
                   totalDemonsAmount += unit.amount
               }
               alert('both armies are out of attacks, battle over.')
               if (totalAlliesAmount > totalDemonsAmount) {
                if (current === world[1]) {
                    current = world[2]
                    return alert('Allies win this battle, next battle will be for the win at demon world')
                }
                else if (current === world[0]) {
                    current = world[1]
                    return alert('The allies have won and successfully deffended their kingdom! next battle will take place at the neutral ground')
                } else {
                    gameWon = true
                    return alert('Allies have defeated the demons and have won the war!!!')
                    
                }
             }
            else if(totalDemonsAmount > totalAlliesAmount) {
                if (current === world[1]) {
                    current = world[0]
                    return alert('Demons win this battle, next battle will be at Allies Kingdom...')
                }
                else if (current === world[2]) {
                    current = world[1]
                    return alert('The Demons have deffended their world... next battle will take place at the neutral ground')
                } else {
                    gameLost = true
                    return alert('The demons have won... We are all fucked...')                   
                }
             }
            }
        }   
    }
}
//=========================CHOOSE ATTACKER AND ATTACK TYPE========================

var chooseAttacker = () => {

    let unitIndex = -1
    let enemyIndex = -1
    let req3 = ''
    
    while (unitIndex == -1) {
        let req = prompt('Choose a unit to attack with')
        unitIndex = armies.findIndex(item => item.name == req)
        if (unitIndex == -1) {
            alert('Unit doesnt exist, try again.')
        }
        else if (armies[unitIndex].attackNum < 1) {
            unitIndex = -1
            alert('Unit out of attacks, try again.')
        }
    }
    while (enemyIndex == -1) {
        let req2 = prompt('who do you want to attack?')
        enemyIndex = demonArmies.findIndex(item => item.name == req2)
        if (enemyIndex == -1) {
            alert('Enemy doesnt exist, try again')
        }
        else if (demonArmies[enemyIndex].dead === true) {
            alert(`${demonArmies[enemyIndex].name} is already dead!`)
            enemyIndex = -1
        }
    }

    if(unitIndex !== -1) {
        while (req3 !== 'physical' && req3 !== 'magic') {
                req3 = prompt('Choose an attack type')
        if (req3 === 'physical') {
            attack(armies[unitIndex], demonArmies[enemyIndex])
            }
        else if (req3 === 'magic') {
            magAttack(armies[unitIndex], demonArmies[enemyIndex])
            }                    
        }
    }
}

//===========================DEMON ATTACK========================================

var demonAttack = () => {

   

    let enemyNumber = Math.round(Math.random() * 10)
    //attack with 3rd demon
        if (demonArmies[0].attackNum < 1 && demonArmies[2].attackNum > 0 && demonArmies[2].dead !== true || 
            demonArmies[0].dead === true && demonArmies[2].dead !== true && demonArmies[2].attackNum > 0 ||
            enemyNumber > 7 && demonArmies[2].attackNum > 0 && demonArmies[2].dead !== true) {
                
            //get damage type
            if(demonArmies[2].attackPower > demonArmies[2].magAttackPower){               
                //choose who to attack
                armies.sort((a, b) => a.defence - b.defence)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (allies[0].dead !== true) {
                        attack(demonArmies[2], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[2], armies[1])
                    } else {
                        attack(demonArmies[2], armies[2])
                    }
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        attack(demonArmies[2], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        attack(demonArmies[2], armies[2])
                    } else {
                        attack(demonArmies[2], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[2], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[2], armies[1])
                    } else {
                        attack(demonArmies[2], armies[0])
                    }
                }
            }
            else {
                armies.sort((a, b) => a.magDef - b.magDef)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (armies[0].dead !== true) {
                        magAttack(demonArmies[2], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        magAttack(demonArmies[2], armies[1])
                    } else {
                        magAttack(demonArmies[2], armies[0])
                    } 
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        magAttack(demonArmies[2], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        magAttack(demonArmies[2], armies[2])
                    } else {
                        magAttack(demonArmies[2], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[2], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[2], armies[1])
                    } else {
                        attack(demonArmies[2], armies[0])
                    }
                }
            }     
        }
        //magicAttack with 2nd demon
        else if (demonArmies[2].attackNum < 1 && demonArmies[1].attackNum > 0 && demonArmies[1].dead !== true ||
                 demonArmies[2].dead === true && demonArmies[1].dead !== true && demonArmies[1].attackNum > 0 || 
                 enemyNumber >= 4 && enemyNumber <= 7 && demonArmies[1].attackNum > 0 && demonArmies[1].dead !== true) { 
                    
               //get damage type
               if(demonArmies[1].attackPower > demonArmies[1].magAttackPower){               
                //choose who to attack
                armies.sort((a, b) => a.defence - b.defence)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (armies[0].dead !== true) {
                        attack(demonArmies[1], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[1], armies[1])
                    } else {
                        attack(demonArmies[1], armies[2])
                    }
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        attack(demonArmies[1], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        attack(demonArmies[1], armies[2])
                    } else {
                        attack(demonArmies[1], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[1], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[1], armies[1])
                    } else {
                        attack(demonArmies[1], armies[0])
                    }
                }
            }
            else {
                armies.sort((a, b) => a.magDef - b.magDef)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (armies[0].dead !== true) {
                        magAttack(demonArmies[1], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        magAttack(demonArmies[1], armies[1])
                    } else {
                        magAttack(demonArmies[1], armies[0])
                    } 
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        magAttack(demonArmies[1], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        magAttack(demonArmies[1], armies[2])
                    } else {
                        magAttack(demonArmies[1], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[1], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[1], armies[1])
                    } else {
                        attack(demonArmies[1], armies[0])
                    }
                }
            }   

        }
        // attack with first demon
        else if (demonArmies[1].attackNum < 1 && demonArmies[0].attackNum > 0 && demonArmies[0].dead !== true ||
                 demonArmies[1].dead === true && demonArmies[0].dead !== true && demonArmies[0].attackNum > 0 || 
                enemyNumber < 4 && demonArmies[0].attackNum > 0 && demonArmies[0].dead !== true) {
                  
              //get damage type
              if(demonArmies[1].attackPower > demonArmies[1].magAttackPower){               
                //choose who to attack
                armies.sort((a, b) => a.defence - b.defence)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (armies[0].dead !== true) {
                        attack(demonArmies[0], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[0], armies[1])
                    } else {
                        attack(demonArmies[0], armies[2])
                    }
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        attack(demonArmies[0], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        attack(demonArmies[0], armies[2])
                    } else {
                        attack(demonArmies[0], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[0], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[0], armies[1])
                    } else {
                        attack(demonArmies[0], armies[0])
                    }
                }
            }
            else {
                armies.sort((a, b) => a.magDef - b.magDef)
                let rng = Math.round(Math.random() * 10)
                if(rng > 8) {
                    if (armies[0].dead !== true) {
                        magAttack(demonArmies[0], armies[0])
                    }
                    else if (armies[1].dead !== true) {
                        magAttack(demonArmies[0], armies[1])
                    } else {
                        magAttack(demonArmies[0], armies[0])
                    } 
                }
                else if (rng > 3) {
                    if (armies[1].dead !== true) {
                        magAttack(demonArmies[0], armies[1])
                    }
                    else if (armies[2].dead !== true) {
                        magAttack(demonArmies[0], armies[2])
                    } else {
                        magAttack(demonArmies[0], armies[0])
                    }
                } else {
                    if (armies[2].dead !== true) {
                        attack(demonArmies[0], armies[2])
                    }
                    else if (armies[1].dead !== true) {
                        attack(demonArmies[0], armies[1])
                    } else {
                        attack(demonArmies[0], armies[0])
                    }
                }
            }   
        }
}

//=====================ATTACK and DEATH===========================================

var attack = (unit1, unit2) => {
    let unit1Att = Math.round(unit1.attackPower * 100)
    let unit2Def = Math.round(unit2.defence * 100)
    let dam = Math.round((20 * (unit1Att / unit2Def)))
    let crit = Math.round(Math.random() * 10)
    
    //check if unit1 is alive and has attacks and unit2 is alive
    if (unit1.dead !== true && unit1.attackNum > 0 && unit2.amount > 0 && unit1.type === 'ranged') {  
        unit1.exp += dam
        unit1.attackNum -=1  
        if (crit > 7) {
            unit2.amount -= dam * 2
            alert(`${unit2.name} has been shot in the face! ${dam * 2} damage dealt.`)
        } else {
            unit2.amount -= dam
            alert(`${unit2.name} has been dealt ${dam} physical damage.`)
        }
        console.log(`${unit2.name} current amount: ${unit2.amount}`)
        console.log(`${unit1.name} has ${unit1.attackNum} attacks left`)
        console.log()
        if (unit2.amount < 1) {
            unit2.dead = true
            console.log(`${unit2.name} is dead!!`)
            alert(`${unit2.name} is dead!!`)
        }   
    }
    else if (unit1.dead !== true && unit1.attackNum > 0 && unit2.amount > 0) {  
        unit1.exp += dam
        unit2.amount -= dam
        unit1.attackNum -=1  
        console.log(`${unit2.name} has been dealt ${dam} physical damage.`)
        console.log(`${unit2.name} current amount: ${unit2.amount}`)
        console.log(`${unit1.name} has ${unit1.attackNum} attacks left`)
        console.log()
        alert(`${unit2.name} has been dealt ${dam} physical damage.`)
        if (unit2.amount < 1) {
            unit2.dead = true
            console.log(`${unit2.name} is dead!!`)
            alert(`${unit2.name} is dead!!`)
        }   
    }
    // fall back on attacker is out of attacks
    else {
        console.log(`'${unit1.name} is out of attacks...`)
    }
}

//==========================MAGIC ATTACK====================================

let magAttack = (unit1, unit2) => {
    let unit1Att = Math.round(unit1.magAttackPower * 100)
    let unit2Def = Math.round(unit2.magDef * 100)
    let dam = Math.round((20 * (unit1Att / unit2Def)))
    
    //check if unit1 is alive and has attacks and unit2 is alive
    if (unit1.dead !== true && unit1.attackNum > 0 && unit2.amount > 0) {  
        unit1.exp += dam
        unit2.amount -= dam
        unit1.attackNum -=1  
        alert(`${unit2.name} has been dealt ${dam} magic damage.`)
        console.log(`${unit2.name} has been dealt ${dam} magic damage.`)
        console.log(`${unit2.name} current amount: ${unit2.amount}`)
        console.log(`${unit1.name} has ${unit1.attackNum} attacks left`)
    if (unit2.amount < 1) {
            unit2.dead = true
            alert(`${unit2.name} is dead!!`)
            console.log(`${unit2.name} is dead!!`)
        } 
    }
    // fall back on attacker is out of attacks
    else {
        console.log(`'${unit1.name} is out of attacks...`)
    }
}

//==========================LVL UP==========================================

var lvlUp = () => {
    for (let unit of armies) {
        if(unit.exp > 100) {
            unit.lvl++
            unit.attackPower +=1
            unit.magicAttackPower += 1
            unit.magDef += 1
            unit.defence +=1
            if(unit.exp > 200) {
                unit.exp = unit.exp - 200
            } else {
                unit.exp = unit.exp - 100
            }
            alert(`${unit.name} has become level ${unit.lvl}`)
            console.log(`${unit.name} has become level ${unit.lvl}!!`)
        }
        if(unit.exp > 100 && unit.lvl !== 1 && unit.lvl % 2 !== 0) {
            unit.lvl++
            unit.attackPower +=1
            unit.magAttackPower += 1
            unit.attackNum += 1
            unit.resetAttacks += 1
            unit.defence +=1
            unit.magDef += 1
            unit.exp = unit.exp - 100
            console.log(`${unit.name} has become level ${unit.lvl}!!`)
            alert(`${unit.name} has become level ${unit.lvl}`)
        }
    }
}

//=============================AFTER BATTLE=================================

// lvl up here and reset demon army, armies, all used keys back to false
// obtain gold
var afterBattle = () => {
    for (let unit of demonArmies) {
        if (unit.dead === true) {
            gold += unit.gold
        }
    }
    demonArmies = []
    demons = defaultDemons
// lvl up units
    for (let unit of armies) {
        if (unit.exp > 100) {
            lvlUp()
        }
// reset values
        if (unit.item1 != false) {
            if(unit.item1.attackBonus) {
                unit.attackPower -= unit.item1.attackBonus
                unit.item1 = false
            }
            else if (unit.item1.magAttBonus) {
                unit.magAttackPower -= unit.item1.magAttBonus
                unit.item1 = false
            }
            else if (unit.item1.defenceBonus) {
                unit.defence -= unit.item1.defenceBonus
                unit.item1 = false
            }
            else if (unit.item1.magDefBonus) {
                unit.magDef -= unit.item1.magDefBonus
                unit.item1 = false
            }
            else if (unit.item1 === 'none') {
                unit.item1 = false
            }
        }
        if (unit.item2 =! false) {
            if(unit.item2.attackBonus) {
                unit.attackPower -= unit.item2.attackBonus
                unit.item2 = false
            }
            else if (unit.item2.magAttBonus) {
                unit.magAttackPower -= unit.item2.magAttBonus
                unit.item2 = false
            }
            else if (unit.item2.defenceBonus) {
                unit.defence -= unit.item2.defenceBonus
                unit.item2 = false
            }
            else if (unit.item2.magDefBonus) {
                unit.magDef -= unit.item2.magDefBonus
                unit.item2 = false
            }
            else if (unit.item2 === 'none') {
                unit.item2 = false
            }
        }
        if (unit.hero != false) {
            unit.hero = false
        }
        if (unit.dead === true) {
            allies.splice(unit, 1)
        }
    }
    for (let item of items) {
        item.used = false
    }
    for (let unit of armies) {
        unit.attackNum = unit.resetAttacks
    }
    armies = []
}

//============================SHOP=========================================

//buy from default allies and push to allies

var shop = () => {

    let finished = false

    for (unit of allies) {
        if (unit.amount <= 50) {
            let req = prompt(`Do you want to heal ${unit.name} to full health for 50 gold, current health is ${unit.amount}, you have ${gold} gold`)
            if (req === 'no') {
                alert(`${unit.name} not healed, current health is ${unit.amount}`)
            }
            else if (req === 'yes') {
                if (gold > 50) {
                unit.amount = 100
                gold -= 50
                alert(`${unit.name} has been healed to full health`)
                } else {
                    alert('You dont have enough gold...')
                }
            }
        }
        else if (unit.amount > 50 && unit.amount < 100) {
            let req2 = prompt(`Do you want to heal ${unit.name} to full health for 25 gold, current health is ${unit.amount}, you have ${gold} gold`)
            if (req2 === 'no') {
                alert(`${unit.name} not healed, current health is ${unit.amount}`)
            }
            else if (req2 === 'yes') {
                if (gold > 25) {
                    unit.amount = 100
                    gold -= 25
                    alert(`${unit.name} has been healed to full health`)
                } else {
                    alert('You dont have enough gold...')
                }
            }
        }
    }
    while (gold > 50 || finished !== true) {
        let req3 = prompt(`Do you want to buy new units, you have ${gold} gold`)
        if (req3 === 'no') {
            alert('No new units bought, going back to pre battle phase')
            finished = true
        }
        else if (req3 === 'yes') {
            console.log(allies)
            // buy from allies and change .have to true
            let req4 = prompt('Which unit do you want to buy?')
            let alliesUnitIndex = allies.findIndex(item => item.name == req4)
            if (alliesUnitIndex !== -1 && allies[alliesUnitIndex].have !== true) {
                alert(`You have bought ${allies[alliesUnitIndex].name}, you can use this unit for the next battle`)
                gold -= allies[alliesUnitIndex].cost
                allies[alliesUnitIndex].have = true
                let req5 = prompt('Do you want to buy another?')
                if (req5 === 'no') {
                    finished = true
                }
            }
            else if (alliesUnitIndex === -1) {
                alert('This unit doesnt exist.')
            }
            else if (alliesUnitIndex !== -1) {
                alert(`You already own this unit.`)
            }
        }
    }
}

//=============================GOOD GAME OVER==============================

var goodGameOver = () => {
    totalWins += 1
    gameWon = true
    return alert('Well done you win :)')
    
}

//=============================BAD GAME OVER==============================

var badGameOver = () => {
    gameLost = true
    totalLosses += 1
    return alert('GAME OVER')
}

//============================RUN GAME=====================================

var startGame = () => {
    alert('Start Game!')
    initialiseWar()
    while (gameWon === false || gameLost === false) {
    chooseArmies()
    equipItems()
    randomDemonArmy()
    alert(`We are ready to start battle ${battleNum}!!`)
    alert(`The war will start at ${current}`)
    battleRound()
    if (gameWon === true) {
        return goodGameOver()
    }
    else if(gameLost === true) {  
        return badGameOver()
    } else {
        afterBattle()
        shop()
        alert('Get ready for next round!!')   
    }
}
}
      
        
        



