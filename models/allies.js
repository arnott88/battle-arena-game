const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const alliesSchema = new Schema({
    unit : {
        type : String,
        require : true,
    },
    amount : {
        type : String,
        require : true
    },
    attackPower : {
        type : String,
        require : true
    },
    magicAttackPower : {
        type : String
    },
    attackSpeed : {
        type : String,
        require : true
    },
    defense : {
        type : String,
        require : true
    },
    magicDefense : {
        type : String
    },
    lvl : {
        type : String,
        require : true
    },
    exp : {
        type : String,
        require : true
    },
    dead : {
        type : Boolean,
        default : false
    },
    image : {
        
    }
})
module.exports =  mongoose.model('allies', alliesSchema);