const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/allies');

    router.post('/add', controller.add)
    router.delete('/remove', controller.remove)

module.exports = router;