const Allies = require('../models/allies.js');

//============================================================================
const add = async (req, res) => {
    try{
    const response = await Allies.create({unit: req.body.unit,
                                         amount: req.body.amount,
                                         attackPower: req.body.attackPower,
                                         magicAttackPower: req.body.magicAttackPower,
                                         attackSpeed: req.body.attackSpeed,
                                         defense: req.body.defense,
                                         magicDefense: req.body.magicDefense,
                                         lvl: req.body.lvl,
                                         exp: req.body.exp,
                                         dead: req.body.dead})
    console.log(response)
    res.send({ok: true, message: `${response} created in products collection`})
    }catch(error) {
        res.send({ok: false, message: `something went wrong`})
    }
}


//==============================================================================
const remove = async(req,res) => {
    try{
       const {_id} = req.body
       const response = await Allies.deleteOne({_id:_id})
       console.log(response)
       res.send({ok:true, message:'Unit removed'})
    } catch {
       res.send({ok:false, message:'something went wrong'})
       }
    }

//==============================================================================
const updateUnit = async (req, res) => {
    const { _id, unit, amount, attackPower, magicAttackPower, attackSpeed,
    defense, magicDefense, lvl, exp, dead } = req.body
    try{
        const xproduct = await Products.findOne ({_id:_id})
        const response = await Products.updateOne({_id:_id}, {
                                                        product:product || xproduct.product, 
                                                        price:price || xproduct.price, 
                                                        color:color || xproduct.color, 
                                                        catId:catId || xproduct.catId
                                                    })
        console.log(response)
        res.send({ok: true, response})
    }catch(error) {
        console.log(error)
        res.send({ok: false, message: `something went wrong`})
    }
}

module.exports = {
add,
remove
}